package com.example.foxdev.com.repository;

import com.example.foxdev.com.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity,Long> {

}
