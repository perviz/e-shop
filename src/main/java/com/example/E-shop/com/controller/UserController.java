package com.example.foxdev.com.controller;

import com.example.foxdev.com.dto.response.ResponseUser;
import com.example.foxdev.com.model.UserEntity;
import com.example.foxdev.com.dto.requestUser.RequestUser;
import com.example.foxdev.com.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user")

public class UserController {

    private final UserServiceImpl userServiceImpl;

    @GetMapping
    public ResponseEntity<List<ResponseUser>> getAllUsers() {

        return ResponseEntity.ok(userServiceImpl.getAllUsers());

    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseUser> getUSerById(@PathVariable Long id) {

        return ResponseEntity.ok(userServiceImpl.getUserById(id));

    }

    @PostMapping
    public ResponseEntity<UserEntity> createUser(@RequestBody RequestUser requestUser) {
        System.out.println(requestUser);
        return ResponseEntity.ok(userServiceImpl.createUser(requestUser));

    }
    @PutMapping("/{id}")
    public ResponseEntity<UserEntity> updateUser(@PathVariable Long id, @RequestBody RequestUser requestUser){
        return ResponseEntity.ok(userServiceImpl.updateUser(id,requestUser));

    }
//    @PatchMapping
//    public ResponseEntity<UserEntity>deactiveUser(@PathVariable Long id){
//        return ResponseEntity.ok(userServiceImpl.deactiveUser(id));
//    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id){
        return ResponseEntity.ok(userServiceImpl.deleteById(id));
    }

}
