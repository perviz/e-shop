package com.example.foxdev.com.service.impl;
import com.example.foxdev.com.dto.response.ResponseUser;
import com.example.foxdev.com.exception.UserNotFoundException;
import com.example.foxdev.com.model.UserEntity;
import com.example.foxdev.com.dto.requestUser.RequestUser;
import com.example.foxdev.com.repository.UserRepository;
import com.example.foxdev.com.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<ResponseUser> getAllUsers() {
        return userRepository.findAll().stream()
                .map(userEntity -> modelMapper.map(userEntity,ResponseUser.class))
                .collect(Collectors.toList());

    }

    @Override
    public ResponseUser getUserById(Long id) {
       UserEntity byId = findUserById(id);
       return modelMapper.map(byId,ResponseUser.class);

    }


    @Override
    public UserEntity createUser(RequestUser requestUser) {
        System.out.println(requestUser);
        UserEntity user = modelMapper.map(requestUser, UserEntity.class);
       return userRepository.save(user);

    }

    @Override
    public UserEntity updateUser(Long id, RequestUser requestUser) {
        UserEntity userEntity = findUserById(id);
        UserEntity user = modelMapper.map(requestUser, UserEntity.class);
        user.setId(id);
        return userRepository.save(user);

    }

//    @Override
//    public UserEntity deactiveUser(Long id) {
//        UserEntity userEntity = findUserById(id);
//        return userRepository.d(id);
//    }

    @Override
    public String deleteById(Long id) {
        UserEntity userEntity = findUserById(id);
         userRepository.deleteById(id);
         return "User with id: "+ id +" is deleted";

    }
    private UserEntity findUserById(Long id){
        return userRepository.findById(id).
                orElseThrow(() -> new UserNotFoundException("User couldn't find with this id "+ id));

    }

}
