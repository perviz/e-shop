package com.example.foxdev.com.service;

import com.example.foxdev.com.dto.response.ResponseUser;
import com.example.foxdev.com.model.UserEntity;
import com.example.foxdev.com.dto.requestUser.RequestUser;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    List<ResponseUser> getAllUsers();

    ResponseUser getUserById(Long id);

    UserEntity createUser(RequestUser requestUser);

    UserEntity updateUser(Long id, RequestUser requestUser);

//    UserEntity deactiveUser(Long id);

    String deleteById(Long id);
}
