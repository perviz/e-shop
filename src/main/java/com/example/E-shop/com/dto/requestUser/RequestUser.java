package com.example.foxdev.com.dto.requestUser;

import lombok.ToString;

@ToString
public class RequestUser {
    private String mail;
    private String firstName;
    private String lastName;
    private String middleName;
}
